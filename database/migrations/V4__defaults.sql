
INSERT INTO "triples"("name", "enabled") VALUES ('generic', true);

INSERT INTO task_kinds(name) VALUES ('metadata');
INSERT INTO task_kinds(name) VALUES ('tarball');
INSERT INTO task_kinds(name) VALUES ('trunk');
INSERT INTO task_kinds(name) VALUES ('coverage');

INSERT INTO default_tasks(kind, priority) VALUES ((SELECT id FROM task_kinds WHERE name = 'metadata'), 10);

INSERT INTO job_stages(name) VALUES ('init');
INSERT INTO job_stages(name) VALUES ('fetch');
INSERT INTO job_stages(name) VALUES ('build');
INSERT INTO job_stages(name) VALUES ('upload');

