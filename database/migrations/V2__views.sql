
CREATE VIEW "tasks_view" AS
    SELECT
        tasks.id AS id,
        tasks.priority AS priority,
        tasks.variant AS variant,
        tasks.jobs AS jobs,
        crates.name AS crate,
        crate_versions.version,
        crate_versions.url,
        task_kinds.name AS kind,
        triples.name AS triple,
        (SELECT count(*) FROM jobs WHERE jobs.task = tasks.id AND jobs.ended IS NULL) AS jobs_running,
        (SELECT count(*) FROM jobs WHERE jobs.task = tasks.id AND jobs.success = TRUE) AS jobs_success
    FROM tasks
    LEFT JOIN triples ON tasks.triple = triples.id
    JOIN task_kinds ON tasks.kind = task_kinds.id
    JOIN crate_versions ON tasks.version = crate_versions.id
    JOIN crates ON crate_versions.crate = crates.id;

CREATE VIEW "tasks_queue" AS
    SELECT
        *
    FROM tasks_view
    WHERE jobs_success = 0
    AND jobs_running = 0
    ORDER BY priority DESC, jobs;

CREATE VIEW "jobs_view" AS
    SELECT
        jobs.*,
        triples.name AS triple_name,
        builders.uuid AS builder_uuid,
        crates.name AS crate_name,
        crate_versions.version AS crate_version_version,
        crate_versions.url AS url
    FROM jobs
    JOIN builders
        ON jobs.builder = builders.id
    JOIN tasks
        ON jobs.task = tasks.id
    LEFT JOIN triples
        ON tasks.triple = triples.id
    JOIN crate_versions
        ON tasks.version = crate_versions.id
    JOIN crates
        ON crate_versions.crate = crates.id;

CREATE VIEW "pubkey_fingerprints_view" AS
    SELECT
        pubkey_fingerprints.fingerprint,
        pubkeys.id,
        pubkeys.encoded
    FROM
        pubkey_fingerprints
    JOIN pubkeys
        ON pubkey_fingerprints.pubkey = pubkeys.id;

CREATE VIEW "builders_view" AS
    SELECT
        pubkeys.encoded AS pubkey,
        builders.id,
        builders.enabled,
        builders.comment,
        builders.uuid
    FROM builders
    JOIN pubkeys
        ON builders.pubkey = pubkeys.id;

CREATE VIEW "builder_triples_view" AS
    SELECT
        builders.id AS builder,
        builders.uuid AS builder_uuid,
        builders.enabled AS builder_enabled,
        builders.comment AS builder_comment,
        triples.id AS triple,
        triples.name AS triple_name
    FROM builder_triples
    JOIN triples
        ON builder_triples.triple = triples.id
    JOIN builders
        ON builder_triples.builder = builders.id;

-- view for registry versions
CREATE VIEW "crate_versions_view" AS
    SELECT
        crates.name,
        crate_versions.*
    FROM crates
    JOIN crate_versions
        ON crates.id = crate_versions.crate;

