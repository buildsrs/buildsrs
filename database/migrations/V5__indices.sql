
CREATE INDEX ON jobs USING btree (task) WHERE (ended IS NULL);

CREATE INDEX ON tasks USING btree (priority DESC, jobs);
