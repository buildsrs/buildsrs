CREATE EXTENSION pg_trgm;

-- ssh public keys
CREATE TABLE "pubkeys" (
    "id" BIGSERIAL PRIMARY KEY,
    "encoded" TEXT NOT NULL UNIQUE
);

-- ssh public key fingerprints
CREATE TABLE "pubkey_fingerprints" (
    "fingerprint" TEXT PRIMARY KEY,
    "pubkey" BIGINT NOT NULL REFERENCES pubkeys(id) ON DELETE CASCADE
);

-- builders that are registered
CREATE TABLE "builders" (
    "id" BIGSERIAL PRIMARY KEY,
    "uuid" UUID NOT NULL UNIQUE,
    "pubkey" BIGINT NOT NULL REFERENCES pubkeys(id) ON DELETE RESTRICT,
    "enabled" BOOLEAN NOT NULL DEFAULT (FALSE),
    "heartbeat" BIGINT,
    "comment" TEXT
);

-- triples that can be built for (example x86_64-unknown-linux-musl)
CREATE TABLE "triples" (
    "id" BIGSERIAL PRIMARY KEY,
    "enabled" BOOLEAN NOT NULL DEFAULT (FALSE),
    "name" TEXT NOT NULL UNIQUE
);

-- triples that are enabled per builder
CREATE TABLE "builder_triples" (
    "builder" BIGINT NOT NULL REFERENCES builders(id) ON DELETE CASCADE,
    "triple" BIGINT NOT NULL REFERENCES triples(id) ON DELETE CASCADE,
    PRIMARY KEY ("builder", "triple")
);

-- crates from the registry
CREATE TABLE "crates" (
    "id" BIGSERIAL PRIMARY KEY,
    "enabled" BOOLEAN NOT NULL DEFAULT (TRUE),
    "name" TEXT NOT NULL UNIQUE
);

-- crate versions
CREATE TABLE "crate_versions" (
    "id" BIGSERIAL PRIMARY KEY,
    "crate" BIGINT NOT NULL REFERENCES crates(id) ON DELETE CASCADE,
    "version" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "checksum" TEXT NOT NULL,
    "yanked" BOOLEAN NOT NULL,
    UNIQUE ("crate", "version")
);

-- kinds of artifacts that can be produced
CREATE TABLE "task_kinds" (
    "id" BIGSERIAL PRIMARY KEY,
    "name" TEXT NOT NULL UNIQUE
);

CREATE TABLE "default_tasks" (
    "id" BIGSERIAL PRIMARY KEY,
    "kind" BIGINT NOT NULL REFERENCES task_kinds(id) ON DELETE RESTRICT,
    "triple" BIGINT REFERENCES triples(id) ON DELETE RESTRICT,
    "variant" VARCHAR,
    "priority" INT NOT NULL DEFAULT (0),
    UNIQUE ("kind", "triple")
);

CREATE TABLE "tasks" (
    "id" BIGSERIAL PRIMARY KEY,
    "version" BIGINT NOT NULL REFERENCES crate_versions(id) ON DELETE RESTRICT,
    "kind" BIGINT NOT NULL REFERENCES task_kinds(id) ON DELETE RESTRICT,
    "triple" BIGINT REFERENCES triples(id) ON DELETE RESTRICT,
    "variant" VARCHAR,
    "priority" INT NOT NULL DEFAULT (0),
    "jobs" INT NOT NULL DEFAULT (0),
    UNIQUE ("version", "kind", "triple")
);

-- job stages
CREATE TABLE "job_stages" (
    "id" BIGSERIAL PRIMARY KEY,
    "name" TEXT NOT NULL UNIQUE
);

-- build jobs and their current status
CREATE TABLE "jobs" (
    "id" BIGSERIAL PRIMARY KEY,
    "task" BIGINT NOT NULL REFERENCES tasks(id) ON DELETE RESTRICT,
    "uuid" UUID NOT NULL UNIQUE,
    "builder" BIGINT NOT NULL REFERENCES builders(id) ON DELETE RESTRICT,
    "started" BIGINT NOT NULL DEFAULT (0),
    "timeout" BIGINT NOT NULL DEFAULT (0),
    "stage" BIGINT NOT NULL REFERENCES job_stages(id) ON DELETE RESTRICT,
    "ended" BIGINT,
    "success" BOOLEAN
);

-- jobs log output
CREATE TABLE "job_logs" (
    "id" BIGSERIAL PRIMARY KEY,
    "job" BIGINT NOT NULL REFERENCES jobs(id) ON DELETE CASCADE,
    "stage" BIGINT NOT NULL REFERENCES job_stages(id) ON DELETE RESTRICT,
    "line" TEXT NOT NULL
);

-- build job artifacts
CREATE TABLE "job_artifacts" (
    "id" BIGSERIAL PRIMARY KEY,
    "job" BIGINT NOT NULL REFERENCES jobs(id) ON DELETE CASCADE,
    "name" TEXT NOT NULL,
    "hash" TEXT NOT NULL,
    "size" BIGINT NOT NULL,
    "signature" TEXT NOT NULL,
    "downloads" BIGINT NOT NULL DEFAULT (0)
);

-- track downloads per artifact
CREATE TABLE "job_artifact_downloads" (
    "artifact" BIGINT NOT NULL REFERENCES job_artifacts(id) ON DELETE CASCADE,
    "date" BIGINT,
    "downloads" BIGINT,
    PRIMARY KEY ("artifact", "date")
);
