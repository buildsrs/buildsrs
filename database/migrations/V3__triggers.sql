
-- Whenever a new crate version is added, create default tasks
CREATE OR REPLACE FUNCTION create_default_tasks()
RETURNS TRIGGER AS $$
DECLARE
    default_task record;
BEGIN
    FOR default_task IN SELECT * FROM default_tasks LOOP
        INSERT INTO tasks(version, kind, triple, variant, priority)
        VALUES (
            NEW.id,
            default_task.kind,
            default_task.triple,
            default_task.variant,
            default_task.priority
        );
    END LOOP;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER create_default_tasks_trigger
AFTER INSERT ON crate_versions
FOR EACH ROW
EXECUTE FUNCTION create_default_tasks();

-- handle insertion on crate_versions_view: do an insert or update.
CREATE OR REPLACE FUNCTION crate_versions_insert()
RETURNS TRIGGER AS $$
BEGIN
    INSERT INTO crate_versions(crate, version, url, checksum, yanked)
    VALUES (
        (SELECT id FROM crates WHERE name = NEW.name),
        NEW.version, NEW.url, NEW.checksum, NEW.yanked
    )
    ON CONFLICT (crate, version) DO UPDATE
    SET
        yanked = NEW.yanked,
        url = NEW.url,
        checksum = NEW.checksum;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER crate_versions_insert_trigger
INSTEAD OF INSERT ON crate_versions_view
FOR EACH ROW EXECUTE FUNCTION crate_versions_insert();

-- update: ensure that checksum is never updated, only update if changed.
CREATE OR REPLACE FUNCTION crate_versions_update()
RETURNS TRIGGER AS $$
BEGIN
    -- cannot change checksum!
    IF OLD.checksum != NEW.checksum THEN
        RAISE EXCEPTION 'changed_checksum';
    END IF;

    IF OLD.url != NEW.url THEN
        RAISE EXCEPTION 'changed_url';
    END IF;

    IF (OLD IS DISTINCT FROM NEW) THEN
        UPDATE crate_versions
        SET yanked = NEW.yanked
        WHERE id = OLD.id;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER crate_versions_update_trigger
INSTEAD OF UPDATE ON crate_versions_view
FOR EACH ROW EXECUTE FUNCTION crate_versions_update();

-- Update tasks table jobs count
CREATE OR REPLACE FUNCTION update_tasks_jobs_count()
RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        UPDATE tasks
        SET jobs = (SELECT COUNT(*) FROM jobs WHERE task = NEW.task)
        WHERE tasks.id = NEW.task;
    ELSIF TG_OP = 'DELETE' THEN
        UPDATE tasks
        SET jobs = (SELECT COUNT(*) FROM jobs WHERE task = OLD.task)
        WHERE tasks.id = OLD.task;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_tasks_jobs_trigger
AFTER INSERT OR UPDATE OR DELETE ON jobs
FOR EACH ROW
EXECUTE FUNCTION update_tasks_jobs_count();
