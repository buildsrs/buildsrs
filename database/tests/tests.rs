use buildsrs_common::entities::{ArtifactKind, Task};
use buildsrs_database::{AnyMetadata, TempDatabase};
use rand_core::OsRng;
use ssh_key::{Algorithm, HashAlg, PrivateKey};
use std::{collections::BTreeSet, future::Future, sync::Arc};
use test_strategy::*;
use uuid::Uuid;

const NUM_CASES: u32 = 10;

async fn with_database<O: Future<Output = ()>, F: Fn(AnyMetadata) -> O>(f: F) {
    let host = std::env::var("DATABASE").expect("DATABASE env var must be present to run tests");
    let temp_database = TempDatabase::create(&host, None).await.unwrap();
    f(Arc::new(temp_database.pool().clone())).await;
    temp_database.delete().await.unwrap();
}

#[tokio::test]
async fn test_statements() {
    with_database(|_pool| async move {}).await;
}

#[proptest(async = "tokio", cases = NUM_CASES)]
async fn can_add_crate(name: String) {
    let name = name.as_str();
    with_database(|metadata| async move {
        // add crate
        let writer = metadata.write().await.unwrap();
        writer.crate_add(&name).await.unwrap();
        writer.commit().await.unwrap();

        // verify presence
        let reader = metadata.read().await.unwrap();
        let info = reader.crate_info(&name).await.unwrap();
        assert_eq!(info.name, name);
        assert!(info.enabled);
    })
    .await;
}

#[proptest(async = "tokio", cases = NUM_CASES)]
async fn can_add_crates(names: Vec<String>) {
    let names = &names;
    with_database(|metadata| async move {
        // add crate
        let writer = metadata.write().await.unwrap();
        for name in names.iter() {
            writer.crate_add(name).await.unwrap();
        }
        writer.commit().await.unwrap();

        // verify presence
        let reader = metadata.read().await.unwrap();
        for name in names.iter() {
            let info = reader.crate_info(&name).await.unwrap();
            assert_eq!(&info.name, name);
            assert!(info.enabled);
        }
    })
    .await;
}

#[proptest(async = "tokio", cases = NUM_CASES)]
async fn can_add_crate_version(name: String, version: String, checksum: [u8; 32], yanked: bool) {
    let name = name.as_str();
    let version = version.as_str();
    with_database(|pool| async move {
        let writer = pool.write().await.unwrap();
        writer.crate_add(name).await.unwrap();
        writer
            .crate_version_add(name, version, "", &checksum, yanked)
            .await
            .unwrap();
        writer.commit().await.unwrap();

        let reader = pool.read().await.unwrap();
        let info = reader.crate_version_info(name, version).await.unwrap();

        assert_eq!(info.name, name);
        assert_eq!(info.version, version);
        //assert_eq!(info.checksum, checksum);
        assert_eq!(info.yanked, yanked);
    })
    .await;
}

#[proptest(async = "tokio", cases = NUM_CASES)]
async fn can_add_crate_version_task(name: String, version: String, checksum: [u8; 32]) {
    let name = name.as_str();
    let version = version.as_str();

    with_database(|pool| async move {
        let writer = pool.write().await.unwrap();
        writer.crate_add(name).await.unwrap();
        writer
            .crate_version_add(name, version, "", &checksum, false)
            .await
            .unwrap();
        writer.commit().await.unwrap();

        let reader = pool.read().await.unwrap();
        let tasks = reader.task_list(None, None, None, None).await.unwrap();
        assert_eq!(
            tasks,
            [Task {
                krate: name.into(),
                version: version.into(),
                kind: ArtifactKind::Metadata,
                triple: None,
                priority: 10,
                variant: None,
            }]
        );
    })
    .await;
}

#[proptest(async = "tokio", cases = NUM_CASES)]
async fn can_yank_crate_version(name: String, version: String, checksum: [u8; 32], yanked: bool) {
    let name = name.as_str();
    let version = version.as_str();
    with_database(|pool| async move {
        let writer = pool.write().await.unwrap();
        writer.crate_add(name).await.unwrap();
        writer
            .crate_version_add(name, version, "", &checksum, yanked)
            .await
            .unwrap();
        writer.commit().await.unwrap();

        for yanked in [true, false] {
            let writer = pool.write().await.unwrap();
            writer
                .crate_version_add(name, version, "", &checksum, yanked)
                .await
                .unwrap();
            writer.commit().await.unwrap();

            let reader = pool.read().await.unwrap();
            let info = reader.crate_version_info(name, version).await.unwrap();

            assert_eq!(info.yanked, yanked);
        }
    })
    .await;
}

#[tokio::test]
async fn can_add_builder() {
    with_database(|pool| async move {
        let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519).unwrap();
        let uuid = Uuid::new_v4();

        let writer = pool.write().await.unwrap();

        // make sure we can add a builder
        writer
            .builder_add(uuid, private_key.public_key(), "comment")
            .await
            .unwrap();
        writer.commit().await.unwrap();

        let reader = pool.read().await.unwrap();

        // get builder
        let builder = reader.builder_get(uuid).await.unwrap();
        assert_eq!(builder.uuid, uuid);
        assert_eq!(&builder.public_key, private_key.public_key());
        assert_eq!(builder.comment, "comment");
    })
    .await;
}

#[tokio::test]
async fn can_lookup_builder() {
    with_database(|pool| async move {
        let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519).unwrap();
        let uuid = Uuid::new_v4();

        // make sure we can add a builder
        let writer = pool.write().await.unwrap();
        writer
            .builder_add(uuid, private_key.public_key(), "comment")
            .await
            .unwrap();
        writer.commit().await.unwrap();

        let reader = pool.read().await.unwrap();
        // make sure we can look it up
        for alg in [HashAlg::Sha256, HashAlg::Sha512] {
            assert_eq!(
                reader
                    .builder_lookup(&private_key.public_key().fingerprint(alg).to_string())
                    .await
                    .unwrap(),
                uuid
            );
        }
    })
    .await;
}

#[tokio::test]
async fn can_set_builder_comment() {
    with_database(|pool| async move {
        let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519).unwrap();
        let uuid = Uuid::new_v4();

        let writer = pool.write().await.unwrap();
        writer
            .builder_add(uuid, private_key.public_key(), "comment")
            .await
            .unwrap();
        writer.commit().await.unwrap();

        for comment in ["this", "that", "other"] {
            let writer = pool.write().await.unwrap();
            // set comment
            writer.builder_set_comment(uuid, comment).await.unwrap();
            writer.commit().await.unwrap();

            // check comment
            let reader = pool.read().await.unwrap();
            let builder = reader.builder_get(uuid).await.unwrap();
            assert_eq!(builder.comment, comment);
        }
    })
    .await;
}

#[tokio::test]
async fn can_set_builder_enabled() {
    with_database(|pool| async move {
        let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519).unwrap();
        let uuid = Uuid::new_v4();

        let writer = pool.write().await.unwrap();
        writer
            .builder_add(uuid, private_key.public_key(), "comment")
            .await
            .unwrap();
        writer.commit().await.unwrap();

        for enabled in [false, true] {
            // set comment
            let writer = pool.write().await.unwrap();
            writer.builder_set_enabled(uuid, enabled).await.unwrap();
            writer.commit().await.unwrap();

            // check comment
            let reader = pool.read().await.unwrap();
            let builder = reader.builder_get(uuid).await.unwrap();
            assert_eq!(builder.enabled, enabled);
        }
    })
    .await;
}

#[proptest(async = "tokio", cases = NUM_CASES)]
async fn can_add_builder_triple(triple: String) {
    let triple = triple.as_str();
    with_database(|pool| async move {
        let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519).unwrap();
        let uuid = Uuid::new_v4();

        let writer = pool.write().await.unwrap();
        writer
            .builder_add(uuid, private_key.public_key(), "comment")
            .await
            .unwrap();
        writer.builder_triple_add(uuid, triple).await.unwrap();
        writer.commit().await.unwrap();

        let reader = pool.read().await.unwrap();

        let triples = reader.builder_triples(uuid).await.unwrap();
        assert!(triples.contains(triple));
    })
    .await;
}

#[proptest(async = "tokio", cases = NUM_CASES)]
async fn can_remove_builder_triple(triple: String) {
    let triple = triple.as_str();
    with_database(|pool| async move {
        let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519).unwrap();
        let uuid = Uuid::new_v4();

        let writer = pool.write().await.unwrap();
        writer
            .builder_add(uuid, private_key.public_key(), "comment")
            .await
            .unwrap();

        writer.builder_triple_add(uuid, triple).await.unwrap();

        let triples = writer.builder_triples(uuid).await.unwrap();
        assert!(triples.contains(triple));

        writer.builder_triple_remove(uuid, triple).await.unwrap();

        let triples = writer.builder_triples(uuid).await.unwrap();
        assert!(!triples.contains(triple));
    })
    .await;
}

#[proptest(async = "tokio", cases = NUM_CASES)]
async fn can_list_builder_triple(triples: BTreeSet<String>) {
    with_database(|pool| async move {
        let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519).unwrap();
        let uuid = Uuid::new_v4();

        let writer = pool.write().await.unwrap();
        writer
            .builder_add(uuid, private_key.public_key(), "comment")
            .await
            .unwrap();
        writer.commit().await.unwrap();

        let reader = pool.read().await.unwrap();

        // defaults to empty
        let triples = reader.builder_triples(uuid).await.unwrap();
        assert!(triples.is_empty());

        drop(reader);

        for triple in triples.iter() {
            let writer = pool.write().await.unwrap();
            writer.builder_triple_add(uuid, triple).await.unwrap();
            writer.commit().await.unwrap();

            let reader = pool.read().await.unwrap();
            let triples = reader.builder_triples(uuid).await.unwrap();
            assert!(triples.contains(triple));
        }
    })
    .await;
}

#[tokio::test]
async fn can_job_create() {
    with_database(|pool| async move {
        let writer = pool.write().await.unwrap();

        // add triple
        let triple = "x86_64-unknown-unknown";
        //writer.triple_add(triple).await.unwrap();

        // add crate and version
        let name = "serde";
        let version = "0.1.0";
        writer.crate_add(name).await.unwrap();
        writer
            .crate_version_add(name, version, "abcdef", &[], false)
            .await
            .unwrap();

        // add builder
        let private_key = PrivateKey::random(&mut OsRng, Algorithm::Ed25519).unwrap();
        let builder = Uuid::new_v4();
        writer
            .builder_add(builder, private_key.public_key(), "comment")
            .await
            .unwrap();
        writer.builder_triple_add(builder, triple).await.unwrap();

        /*
        // add job
        let job = writer.job_request(builder).await.unwrap();

        writer.commit().await.unwrap();

        let reader = pool.read().await.unwrap();
        // get job info
        let info = reader.job_info(job).await.unwrap();

        assert_eq!(info.builder, builder);
        assert_eq!(info.triple, triple);
        assert_eq!(info.name, name);
        assert_eq!(info.version, version);
        */
    })
    .await;
}
