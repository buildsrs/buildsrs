//! # Message enumeration that can be sent by either side

use crate::types::*;
use serde::{Deserialize, Serialize};

/// Message sent by the server.
#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(tag = "message")]
pub enum ServerMessage {
    /// Challenge request for authentication.
    ChallengeRequest(Challenge),
    /// New job response.
    JobResponse(Job),
    /// Currently pending jobs.
    JobList(JobList),
}

/// Messages which can be sent by the client.
#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(tag = "message")]
pub enum ClientMessage {
    /// Initialize connection
    Hello(ClientHello),
    /// Respond to challenge
    ChallengeResponse(Challenge),
    /// Request job
    JobRequest(JobRequest),
}
